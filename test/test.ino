#include <Arduino.h>
#include <stdint.h>

#define BAUD_RATE 115200

typedef uint8_t tUInt8;
typedef uint16_t tUInt16;
typedef boolean tBool;
typedef char tChan;
typedef float tFloat;


tUInt8  ui8_Temperature = 0;
tUInt8  ui8_Humidity = 0;
tUInt8  ui8_WaterLevel = 0;
tBool   b_IsPumpActive = false;
tBool   b_MoistureSensor1 = false;
tBool   b_MoistureSensor2 = false;
tBool   b_MoistureSensor3 = false;
tUInt16 ui16_Altitude = 0;
tUInt8  ui8_WindSpeed = 0;
tUInt16 ui16_CO = 0;
tUInt16 ui16_AtmPressure  = 0;
const int led = 13;
int incomingByte = 0;
tBool b_Flag = false;

void setup()
{
  char buf[100];
  pinMode(led , OUTPUT);
  Serial.begin(BAUD_RATE);
  sprintf(buf, "initialization successfull\n");
  Serial.print(buf);
}

void loop()
{
  char buf[100];
  ui8_Temperature = random(25, 30);
  ui8_Humidity = random(55, 65);
  ui8_WindSpeed = random(10, 20);
  ui16_CO = random(0, 1075);
  ui8_WaterLevel = random(0, 80);
  b_MoistureSensor1 = true;
  b_MoistureSensor2 = false;
  b_MoistureSensor3 = true;
  ui16_Altitude = 220;
  ui16_AtmPressure = random(100, 200);
  // d decimal, f floating, x hexadecimal, s string,
  sprintf(buf, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n", b_IsPumpActive, ui8_Temperature, ui8_Humidity,
          ui8_WaterLevel, b_MoistureSensor1, b_MoistureSensor2, b_MoistureSensor3, ui8_WindSpeed, ui16_AtmPressure, ui16_CO, ui16_Altitude);
  Serial.print(buf);
  delay(500);
  if (Serial.available() > 0)
  {
    // read the incoming byte:
    incomingByte = Serial.read();
    Serial.println(incomingByte);
    if (!b_Flag && incomingByte == 54) {
      b_Flag = true;
      digitalWrite(led, HIGH);
      delay(100);
      digitalWrite(led, LOW);
      delay(100);
      digitalWrite(led, HIGH);
      delay(100);
      digitalWrite(led, LOW);
      delay(100);
    }
    else if (b_Flag && incomingByte == 54)
    {
      b_Flag = false;
    }
  }
}
