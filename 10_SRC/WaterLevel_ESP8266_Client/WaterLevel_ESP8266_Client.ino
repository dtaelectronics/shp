/* ****************************
   @brief

   @author Gerald Cezy Emvoutou

   @date 2022-01-02

   @copyright Digital Transformation Alliance

*/

#include <ESP8266WiFi.h>
#include "Utilities.h"
#include <HCSR04.h>

const tChar* ssid = "WiFi_CleverTree";
const tChar* passwd = "02655826";
const tChar* host = "192.168.1.200";
const tUInt16 port = 50011;

//change the local ip for each ESP8266
IPAddress local_IP(192, 168, 1, 210);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

UltraSonicDistanceSensor distanceSensor(TRIGGER_PIN, ECHO_PIN);
WiFiClient m_c_WiFiClient;
tBool b_IsConnected = false;
tBool b_IsPumpActivated = false;
String header;
String output5State = "off";

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  setup

  ----------------------------------------------------------------
  @parameter:     --
  @Returnvalue:
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid setup()
{
  Serial.begin(BAUDRATE);
  pinMode(RELAI_PIN, OUTPUT);
  Serial.println("Client");
  Serial.println("Connecting to Network");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passwd);

  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  } else {
    Serial.println("STA succesfully configured");
  }

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(100);
  }
  // Relai pin close
  digitalWrite(RELAI_PIN, LOW);
  Serial.print("\n [CLIENT1 CONNECT] ");
  Serial.println(WiFi.localIP());

}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  loop

  ----------------------------------------------------------------
  @parameter:     --
  @Returnvalue:
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid loop()
{

  tChar buf[10];
  tUInt16 ui16_Counter = 0;
  if (!b_IsConnected) {
    while (!m_c_WiFiClient.connect(host, port)) {
      Serial.println("Waiting to connect");
      delay(500);
    }
    b_IsConnected = true;
    Serial.println("Connection successfully");
  }

  tUInt8 ui8_DistanceInCm = ui8_CalculatePercent();

  sprintf(buf, "%d", ui8_DistanceInCm);
  v_MessageHandler(buf);
  // When b_IspumpActivated is true, the activates the pum until counter is less than 5000
  while (b_IsPumpActivated && (ui16_Counter < 65000))
  {
    if (ui8_DistanceInCm >= 10)
    {
      if (ui16_Counter < 1)
      {
        Serial.println("[WaterLevel_ESP8266] Pump is activated");
      }
      digitalWrite(RELAI_PIN, HIGH);
      ++ui16_Counter;
      Serial.println(ui16_Counter);
    }

  }

  // Set the flag to false and stop the pump
  b_IsPumpActivated = false;
  digitalWrite(RELAI_PIN, LOW);
  Serial.println("[WaterLevel_ESP8266] Pump is closed");
  delay(1000);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  send _Message with the current Moisture
  sensor value to Server
  ----------------------------------------------------------------
  @parameter:   String buf
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid v_MessageHandler(String buf)
{
  tChar recv_buf[2];
  String line;
  m_c_WiFiClient.print(buf);

  while (m_c_WiFiClient.available())
  {
    line = m_c_WiFiClient.readStringUntil('\r');
  }
  sprintf(recv_buf, "%s", line);
  if (recv_buf[0] == '1')
  {
    b_IsPumpActivated = true;
  }
  else
  {
    b_IsPumpActivated = false;
  }
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  calcute the distance to the obstacle
  ----------------------------------------------------------------
  @parameter:   String buf
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tUInt8 ui8_RetVal = 0;
tUInt8 ui8_CalculatePercent()
{
  tUInt16 ui16_DistanceInCm = distanceSensor.measureDistanceCm();
  if (ui16_DistanceInCm >= 42)
  {
    ui8_RetVal = 0;
  }
  else if (ui16_DistanceInCm < 42 && ui16_DistanceInCm >= 38)
  {
    ui8_RetVal = 10;
  }
  else if (ui16_DistanceInCm < 38 && ui16_DistanceInCm >= 34)
  {
    ui8_RetVal = 20;
  }
  else if (ui16_DistanceInCm < 34 && ui16_DistanceInCm >= 30)
  {
    ui8_RetVal = 30;
  }
  else if (ui16_DistanceInCm < 30 && ui16_DistanceInCm >= 27)
  {
    ui8_RetVal = 40;
  }
  else if (ui16_DistanceInCm < 27 && ui16_DistanceInCm >= 24)
  {
    ui8_RetVal = 50;
  }
  else if (ui16_DistanceInCm < 24 && ui16_DistanceInCm >= 22)
  {
    ui8_RetVal = 60;
  }
  else if (ui16_DistanceInCm < 22 && ui16_DistanceInCm >= 20)
  {
    ui8_RetVal = 70;
  }
  else if (ui16_DistanceInCm < 20 && ui16_DistanceInCm >= 18)
  {
    ui8_RetVal = 80;
  }
  else if (ui16_DistanceInCm < 18 && ui16_DistanceInCm >= 15)
  {
    ui8_RetVal = 90;
  }
  else if (ui16_DistanceInCm < 15)
  {
    ui8_RetVal = 100;
  }
  return ui8_RetVal;
}
