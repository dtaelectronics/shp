#include "ESP8266Handler.h"


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  ESP8266Handler
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
ESP8266Handler::ESP8266Handler( IPAddress c_IPAddressLocalIP )
: m_pc_ssid(sSID)
, m_pc_password(PSSWD)
, m_pc_host(HOST)
, m_ui16_port(PORT)
, m_c_IPAddressGateway(GATEWAY_ADDR)
, m_c_IPAddressSubnet(SUBNET_ADDR)
, m_c_IPAddressLocalIP(c_IPAddressLocalIP)
, m_pc_WiFiClient(NULL)
, m_b_IsConnect(false)
{

}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tBool ESP8266Handler::b_InitESP8266()
{
    tBool b_RetVal = false;
    Serial.begin(BAUDRATE);
    Serial.println("[ESP8266Handler:] Client");
    Serial.println("[ESP8266Handler:] Connecting to Network");
    WiFi.mode(WIFI_STA);
    WiFi.begin(m_pc_ssid, m_pc_password);

    if (!WiFi.config(m_c_IPAddressLocalIP, m_c_IPAddressGateway, m_c_IPAddressSubnet)) {
        Serial.println("[ESP8266Handler:] STA Failed to configure");
    }else{
        Serial.println("[ESP8266Handler:] STA succesfully configured");
    }

    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(200);
    }
    Serial.print("\n [ESP8266Handler:] NEW CLIENT CONNECTION ");
    Serial.println(WiFi.localIP());
    b_RetVal =  true;

    return b_RetVal;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  b_IsConnect
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tBool ESP8266Handler::b_IsConnect(tBool& b_IsConnect)
{
    if(NULL != m_pc_WiFiClient)
    {
        while(!m_pc_WiFiClient->connect(m_pc_host, m_ui16_port)) {
            Serial.println("[ESP8266Handler:]Waiting to connect");
        }
        Serial.println("[ESP8266Handler:]Connection successfully");
        m_b_IsConnect = true;
    }
    else
    {
        Serial.println("[ESP8266Handler:]Wifi Client not instantianed");
    }
    b_IsConnect = m_b_IsConnect;
    return m_b_IsConnect;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  v_InitClient
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid ESP8266Handler::v_InitClient(WiFiClient c_WiFiClient)
{
    m_pc_WiFiClient = &c_WiFiClient;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  v_SendDatatoServer
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid ESP8266Handler::v_SendDataToServer(tString s_DataToSend, tBool& b_IsConnect)
{
    Serial.println("[ESP8266Handler:] Start sending data from client to Server ...");

    if(NULL != m_pc_WiFiClient)
    {
        m_pc_WiFiClient->print(s_DataToSend);
        unsigned long u32_MsTimer = millis();
        while(m_pc_WiFiClient->available() == 0)
        {
            if(millis() - u32_MsTimer > 5000)
            {
                Serial.println("[ESP8266Handler:] Timeout");
                m_b_IsConnect = false;
                return;
            }
        }
        Serial.println("[ESP8266Handler:] Sending closed.");
    }
    else
    {
        Serial.println("[ESP8266Handler:] Wifi Client not instantianed");
    }
    b_IsConnect = m_b_IsConnect;
}
