/**
 * @file         Arduino_VoltageMeasurement.ino
 * @author       Gerald Emvoutou
 * @version      V1.0
 * @date         2022/05/22
 * @description  this file is sample code to measure the voltage at an analog Pin
 */

#include <Arduino.h>
#include <stdint.h>


uint16_t ui16_ReadAnalagPinA0 = 0;
 
 // setup
void setup() 
{
  Serial.begin(9600);
}

// loop
void loop() {

  ui16_ReadAnalagPinA0 = analogRead(A0);
  Serial.print(" A/D-Value: ");
  Serial.println(ui16_ReadAnalagPinA0);

  float tension = ui16_ReadAnalagPinA0 * 0.0049;
  float tensionMV = ui16_ReadAnalagPinA0 * 4.9;
  Serial.print("Voltage: ");
  Serial.print(tension);
  Serial.print(" V, ");
  Serial.print(tensionMV);
  Serial.println(" mV");
}
 
