/**
 * @file         Arduino_VoltageMeasurementPWM.ino
 * @author       Gerald Emvoutou
 * @version      V1.0
 * @date         2022/05/22
 * @description  this file is sample code to measure the voltage at an analog Pin
 */

#include <Arduino.h>
#include <stdint.h>

#define PWM_PIN     3

uint8_t ui8_AnalogPin = 0b11101111;
 
 // setup
void setup() 
{
  Serial.begin(9600);
}

// loop
void loop() {

  // analogWrite value range 0 .... 255
  analogWrite(PWM_PIN, ui8_AnalogPin);
  Serial.print(" A/D-Value: ");
  Serial.println(ui8_AnalogPin);

  float tension = ui8_AnalogPin * 0.0049 * 4;
  float tensionMV = ui8_AnalogPin * 4.9 * 4;
  Serial.print("Voltage: ");
  Serial.print(tension);
  Serial.print(" V, ");
  Serial.print(tensionMV);
  Serial.println(" mV");
}
 
