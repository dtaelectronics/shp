#include "Arduino.h"
#include "AnalogPin.h"
#include "Utilities.h"

class SoilMoistureSensor
{
private:
    AnalogPin m_c_AnalogPin;
public:
    SoilMoistureSensor(tUInt8 ui8_AnalogPin);
    ~SoilMoistureSensor();
    tString s_ReadDataFromSensor();
};

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  SoilMoistureSensor
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  constructor
----------------------------------------------------------------
@parameter:     --
@Returnvalue:   --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
SoilMoistureSensor::SoilMoistureSensor(tUInt8 ui8_AnalogPin)
: m_c_AnalogPin(ui8_AnalogPin)
{

}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  ~SoilMoistureSensor
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  destructor
----------------------------------------------------------------
@parameter:     --
@Returnvalue:   --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
SoilMoistureSensor::~SoilMoistureSensor()
{

}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  s_ReadDataFromSensor
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  Reads value from Soil moisture
sensor and convert it to string
----------------------------------------------------------------
@parameter:     --
@Returnvalue:   tString s_RetVal
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tString SoilMoistureSensor::s_ReadDataFromSensor()
{
    tString s_RetVal = (tString)m_c_AnalogPin.ui16_ReadAnalogPin();
    return s_RetVal;
}
