/* ****************************
   @brief

   @author Gerald Cezy Emvoutou

   @date 2022-01-02

   @copyright Digital Transformation Alliance

*/

#include <ESP8266WiFi.h>
#include "Utilities.h"

const tChar* ssid = "WiFi_CleverTree";
const tChar* passwd = "02655826";
const tChar* host = "192.168.1.200";
const tUInt16 port = 50011;

//change the local ip for each ESP8266
IPAddress local_IP(192, 168, 1, 203);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

tUInt16 ui16_MoistureSensor = 0;
WiFiClient m_c_WiFiClient;
tBool b_IsConnected = false;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  setup

  ----------------------------------------------------------------
  @parameter:     --
  @Returnvalue:
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid setup()
{
  Serial.begin(BAUDRATE);
  Serial.println("Client");
  Serial.println("Connecting to Network");
  pinMode(LED_PIN, OUTPUT);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passwd);

  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  } else {
    Serial.println("STA succesfully configured");
  }

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(100);
  }

  Serial.print("\n [CLIENT1 CONNECT] ");
  Serial.println(WiFi.localIP());
  delay(10000); // wait 10 second
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  loop

  ----------------------------------------------------------------
  @parameter:     --
  @Returnvalue:
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid loop()
{
  tChar buf[1];

  if (!b_IsConnected) {
    while (!m_c_WiFiClient.connect(host, port)) {
      Serial.println("Waiting to connect");
      delay(500);
    }
    b_IsConnected = true;
    Serial.println("Connection successfully");
  }

  ui16_MoistureSensor = analogRead(SOILMOISTURE_ANALOGPIN);
  Serial.print("ui16_MoistureSensor: ");
  Serial.println(ui16_MoistureSensor);
  if (ui16_MoistureSensor > CALIBRATE_VALUE)
  {
    sprintf(buf, "1,%d",ui16_MoistureSensor );
  }
  else
  {
    sprintf(buf, "0,%d",ui16_MoistureSensor);
  }

  v_ClientSendMessage(buf);
  delay(1000);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  send _Message with the current Moisture
  sensor value to Server
  ----------------------------------------------------------------
  @parameter:   String buf
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid v_ClientSendMessage(String s_buf)
{
  m_c_WiFiClient.print(s_buf);
}
