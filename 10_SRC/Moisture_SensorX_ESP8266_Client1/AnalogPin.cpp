#include "AnalogPin.h"

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function: AnalogPin
@Author: Gerald Emvoutou | Digital Transformation & Technology 
                            Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  Constructor

----------------------------------------------------------------
@parameter:     tUInt8 ui8_AnalogPin
@Returnvalue:   --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
AnalogPin::AnalogPin(tUInt8 ui8_AnalogPin)
: m_ui8_AnalogPin(ui8_AnalogPin)
{

}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  ui16_ReadAnalogPin
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  Reads value from analog pin Arduino

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   tUInt16 ui16_RetVal
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tUInt16 AnalogPin::ui16_ReadAnalogPin()
{
    tUInt16 ui16_RetVal = analogRead(m_ui8_AnalogPin);
    return ui16_RetVal;
}