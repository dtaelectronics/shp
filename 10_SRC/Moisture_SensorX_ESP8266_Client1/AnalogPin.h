/*
* This Class instanzied a Analog Pin 
*/

#include "Arduino.h"
#include "Utilities.h"


class AnalogPin
{

    public:
        AnalogPin(tUInt8 ui8_AnalogPin);
        tUInt16 ui16_ReadAnalogPin();

    private:
        tUInt8 m_ui8_AnalogPin;

};