#include <stdint.h>
#include <stdio.h>

#define BAUDRATE                115200
#define sSID                    "TP-Link_527E"
#define PSSWD                   "02655826"
#define HOST                    "192.168.1.200"
#define PORT                    50011
#define SOILMOISTURE_ANALOGPIN  A0
#define LED_PIN                 D0
#define CALIBRATE_VALUE         150
#define GATEWAY_ADDR            (192, 168, 1, 1)
#define SUBNET_ADDR             (255, 255, 255, 0)
#define TIMEOUT                 30000
#define TIMER_5_MIN             3000//00
#define TIMER_20_MS             20

typedef uint8_t     tUInt8;
typedef uint16_t    tUInt16;
typedef boolean     tBool;
typedef uint32_t    tUInt32;
typedef uint64_t    tUInt64;
typedef char        tChar;
typedef void        tVoid;
typedef String      tString;
