import socket
import time

HOST = socket.gethostbyname(socket.gethostname())  # The server's hostname or IP address
PORT = 50011  # The port used by the server

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    while True:
        s.sendall(b"23.65,33.47")
        data = s.recv(1024)

        print(f"Received {data!r}")
        time.sleep(30)