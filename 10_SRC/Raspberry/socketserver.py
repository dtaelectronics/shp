import socket
import threading
import time
import os


HEADER = 64
SERVER = socket.gethostbyname(socket.gethostname())
PORT1 = 50011
ADDR = (SERVER, PORT1)
FORMAT = 'utf-8'
DISCONNECT = "break"
CLIENT_MOIS_SENSOR0 = "192.168.1.201"
CLIENT_MOIS_SENSOR1="192.168.1.202"
CLIENT_MOIS_SENSOR2="192.168.1.203"
CLIENT_PUMP = "192.168.1.210"
CLIENT_LCDDISPLAY_DHT11 = "192.168.1.211"
CLIENT_DASHBOARD = "192.168.1.101"
CLIENT_WEBSERVER = "192.168.1.50"
CLIENT_SUNLIGHT = "192.168.1.204"

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)


# [b_PumpActivated, s_WaterLevel, s_MoistureSensor0, s_MoistureSensor1, s_MoistureSensor2, DHT11Data, SunlightSensorData]
wp = ["0","0","0","0","0","0.00,0.00","0,0.0000,0,0,0,0"]
def handle_client(conn, addr):

    connected = True
    while connected:
        s_IncomingMsg = conn.recv(HEADER).decode(FORMAT)
        print("[s_IncomingMsg]: " + s_IncomingMsg)
        # reads data from ESP Client and send acknowledge
        
        if (CLIENT_MOIS_SENSOR0 == addr[0]):
            s_MoistureSensor0 = s_IncomingMsg[2:len(s_IncomingMsg)]
            b_PumpActivated = s_IncomingMsg[0:1]
            s_OutComingMsg = "acknowledge client mois sensor 0"
            wp[0] = b_PumpActivated
            wp[2] = s_MoistureSensor0
            conn.send(s_OutComingMsg.encode(FORMAT))
            #print("[CLIENT_MOIS_SENSOR0] "+s_MoistureSensor0)

        elif (CLIENT_MOIS_SENSOR1 == addr[0]):
            s_MoistureSensor1 = s_IncomingMsg[2:len(s_IncomingMsg)]
            b_PumpActivated = s_IncomingMsg[0:1]
            s_OutComingMsg = "acknowledge client mois sensor 1"
            wp[0] = b_PumpActivated
            wp[3] = s_MoistureSensor1
            conn.send(s_OutComingMsg.encode(FORMAT))
            #print("[CLIENT_MOIS_SENSOR1] "+s_MoistureSensor1)

        elif (CLIENT_MOIS_SENSOR2 == addr[0]):
            s_MoistureSensor2 = s_IncomingMsg[2:len(s_IncomingMsg)]
            b_PumpActivated = s_IncomingMsg[0:1]
            wp[0] = b_PumpActivated
            wp[4] = s_MoistureSensor2
            #print("[CLIENT_MOIS_SENSOR2] "+s_MoistureSensor2)

        elif(CLIENT_PUMP == addr[0]):
            s_WaterLevel = s_IncomingMsg
            s_OutComingMsg = wp[0]
            wp[1] = s_WaterLevel
            #print("mgs1 to print "+ s_OutComingMsg)
            #print("waterlevel: "+ s_WaterLevel)
            #activate the pump if the moisture level is low 
            conn.send(s_OutComingMsg.encode(FORMAT))

        elif(CLIENT_LCDDISPLAY_DHT11 == addr[0]):
            DHT11Data = s_IncomingMsg
            wp[5] = DHT11Data
            s_OutComingMsg = wp[0] +","+wp[1]+","+wp[2]+","+wp[3]+","+wp[4]+","+wp[5]+"," + wp[6] 
            conn.send(s_OutComingMsg.encode(FORMAT))

        elif(CLIENT_SUNLIGHT == addr[0]):
            SunlightSensorData = s_IncomingMsg
            wp[6] = SunlightSensorData
            s_OutComingMsg = "acknowledge client sunlight"
            conn.send(s_OutComingMsg.encode(FORMAT))
        
        if s_IncomingMsg == DISCONNECT:
            connected = False
        print("[b_PumpActivated] "+wp[0] )
        print("[s_WaterLevel] "+wp[1] )
        print("[CLIENT_MOIS_SENSOR0] "+wp[2] )
        print("[CLIENT_MOIS_SENSOR1] "+wp[3] )
        print("[CLIENT_MOIS_SENSOR2] "+wp[4] )
        print("[DHT11Data] "+wp[5] )
        print("[SunlightSensorData] "+wp[6] )
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")


def start():
    print("[SERVER ADDRESS]: " + str(SERVER) + ":"+str(PORT1))
    server.listen()
    while True:
        conn, addr = server.accept()
        print(addr)
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.active_count() -1}")
        time.sleep(1)
    server.close()

print("[START] Server is starting and waiting to connection" )
start()