
/* ****************************
   @brief

   @author Gerald Cezy Emvoutou

   @date 2022-01-02

   @copyright Digital Transformation Alliance

*/

#include <ESP8266WiFi.h>
#include "Utilities.h"

const tChar* ssid = "WiFi_CleverTree";
const tChar* passwd = "02655826";
const tUInt16 port = 50011;

IPAddress local_IP(192, 168, 1, 220);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

tUInt8 ui8_IncomingByte = 0;
WiFiServer m_c_WiFiServer(port);
tBool b_IsConnected = false;
tBool b_Flag = false;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  setup

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid setup()
{
  Serial.begin(BAUDRATE);

  WiFi.mode(WIFI_STA);
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, passwd);

  if (!WiFi.config(local_IP, gateway, subnet)) 
  {
    Serial.println("STA Failed to configure");
  }
  else
  {
    Serial.println("STA succesfully configured");
  }

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(100);
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  m_c_WiFiServer.begin();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  loop

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid loop()
{
  v_ClientRcvMessage();
  delay(1000);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 24.04.2022
----------------------------------------------------------------
@Function Description:  reads _Message from Client
----------------------------------------------------------------
@parameter:   --
@Returnvalue:   --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


tVoid v_ClientRcvMessage()
{
  tUInt8 ui8_Index = 0;
  tChar buf[100];
  char c;
  tBool b_IsEnded = false;
  tBool b_IsPumpActive = buf[0];

  WiFiClient m_c_WiFiClient = m_c_WiFiServer.available();
  if(m_c_WiFiClient)
  {
    while(!b_IsEnded && m_c_WiFiClient.connected())
    {
      if(m_c_WiFiClient.available())
      {
        c = m_c_WiFiClient.read();
        Serial.write(c);
        buf[ui8_Index] += c;
        ui8_Index++;
      }
      if(c == '\r')
      {
        b_IsEnded = true;
      }
    }
    Serial.println(buf);

   /* if (Serial.available() > 0)
    {
      // read the incoming byte:
      ui8_IncomingByte = Serial.read();
      if (!b_Flag && ui8_IncomingByte == 54) {
        b_Flag = true;
      }
      else if (b_Flag && ui8_IncomingByte == 54)
      {
        b_Flag = false;
      }

      // pump switch from 0 to 1 
      // start the pump manually if it is closed
      if(!b_IsPumpActive && b_Flag)
      {
        b_IsPumpActive = b_Flag;
      }

      // pump switch from 1 to 0 
      // close the pump manually if it is already open
      else if(b_IsPumpActive && !b_Flag)
      {
        b_IsPumpActive = b_Flag;
      }
      
    }
    buf[0] = b_IsPumpActive;
    Serial.println(buf);*/
  }

}
