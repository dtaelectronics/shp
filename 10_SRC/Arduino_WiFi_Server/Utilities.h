#include <stdint.h>
#include <stdio.h>

#define BAUDRATE                115200
#define sSID                    "TP-Link_527E"
#define PSSWD                   "02655826"
#define HOST                    "192.168.1.200"
#define PORT                    50011
#define DHT_TYPE                11
#define DHT11_PIN               3
#define ECHO_PIN                4 
#define RELAI_PIN               5
#define LIGHT_SENSOR            A6
#define SOUND_VELOCITY          0.034
#define CM_TO_INCH              0.393701
#define LED_PIN                 D0
#define GATEWAY_ADDR            (192, 168, 1, 1)
#define SUBNET_ADDR             (255, 255, 255, 0)
#define TIMEOUT                 30000
#define TIMER_5_MIN             3000//00
#define TIMER_20_MS             20

typedef uint8_t     tUInt8;
typedef uint16_t    tUInt16;
typedef boolean     tBool;
typedef uint32_t    tUInt32;
typedef uint64_t    tUInt64;
typedef char        tChar;
typedef void        tVoid;
typedef String      tString;
typedef float       tFloat;
