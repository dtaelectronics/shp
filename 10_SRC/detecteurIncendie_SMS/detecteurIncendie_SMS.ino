#include<SoftwareSerial.h>

const int led_pin_fonction = 3;
const int led_pin_danger = 4;
const int inputfumee_sensor_pin = 6;
const int inputflamme_sensor_pin =7 ;
const int alarme = 5;

SoftwareSerial mySerial(9,10);
int SMSfumee = 0;
int SMSflamme = 0;


const int powerfumee_sensor_pin = 1;
const int powerflamme_sensor_pin = 2;

float const seuil = HIGH;
float const seuill = LOW;

int flamme_Set = 0;
int fumee_Set = 0;

float a;float b;float c;float d;

void setup(){
    mySerial.begin (19200);

    pinMode (led_pin_danger, OUTPUT);  //configuration pin en sortie
    pinMode (led_pin_fonction, OUTPUT);
    digitalWrite (led_pin_fonction, HIGH);
    
    pinMode (inputfumee_sensor_pin, INPUT);   //configuration pin en entree
    pinMode (powerfumee_sensor_pin, OUTPUT);  //configuration pin en sortie
    digitalWrite (powerfumee_sensor_pin, HIGH);

    pinMode (inputflamme_sensor_pin, INPUT); //configuration pin en entree
    pinMode (powerflamme_sensor_pin, OUTPUT);    //configuration pin en sortie
    digitalWrite (powerflamme_sensor_pin, HIGH);  //nous donnons un etat de sortie
    
    pinMode (alarme, OUTPUT);  //configuration pin en sortie

    mySerial.begin()
    delay(1000);

    Serial.println("composants initialise");
}

void loop()
{
    detecteurincendi();
    detecteurincendietient();
}

void detecteurincendi() //declarer une fonction principal 
{
    detecteurflamme();
    detecteurfumee();
}

void detecteurincendietient()   //declarer une fonction principal
{
    if(flamme_Set==1)
    {
        detecteurflammeeteint();    //declarer une fonction pour detecter si la flamme est eteint
    }
    else if(fumee_Set==1)
    {
        detecteurfumeearretee();  //declarer une fonction pour detecter si la fumee est arrete
    }
}
///////////         a revoir
void detecteurflamme()
{
    int flamme = digitalRead(inputflamme_sensor_pin);
    Serial.println(flamme);

    a = flamme;

    if(a==seuil)
    {
        digitalWrite(alarme, HIGH);
        Alertflamme();
        flamme_Set = 1;
    }
    delay(1000);
}
void detecteurfumee()
{
    int fumee = digitalRead(A0);
    Serial.println(fumee);

    b = fumee;
    if(b==seuil)
    {
        digitalWrite(alarme, HIGH);
        Alertfumee();

        fumee_Set=1;
    }
}

void Alertfumee()
{
    digitalWrite(led_pin_danger, HIGH);
    affichagefumee();

    while(SMSfumee <2)
    {
        messagedefumee();
        SMSfumee++
    }

}

void Alertflamme()
{
    digitalWrite(led_pin_danger, HIGH);
    affichageflamme();

    while(SMSflamme<52)
    {
        messagedeflamme();
        SMSflamme++;
    }
}

void affichagefumee()
{
    Serial.println("fumee au point!");
    delay(200);
    Serial.println("Sending SMS");
    delay(1000);
}

void affichageflamme()
{
    Serial.println("Flamme au point");
    delay(2000);
    Serial.println("Sending SMS");
    delay(1000);
}
 
void messagedefumee()
{
    Serial.println("ATtt+CMGF=111");
    delay(1000);
    Serial.println("AT+CMGS=\"+237675631905\"\r");
    delay(1000);

    Serial.println("Attention Danger");
    delay(1000);
    Serial.println("Fumée détecter dans la maison");
    delay(1000);
    Serial.println((char)26);
    delay(2000);

    Serial.println("AT+CMGS=\"+237675631905\"\r")
    delay(1000);
    Serial.println("")
}

void messagedeflamme()
{

    Serial.println("AT+CMGF=123");
    delay(1000);
    Serial.println("AT+CMGS=\"+237675631905\"\r");

    Serial.println("Attention Danger");
    delay(1000);
    Serial.println("flamme détecter dans la maison");
    delay(1000);
    Serial.println((char)26);
    delay(2000);

    Serial.println("AT+CMGS=\"+237675631905\"\r");
    delay(1000);
    Serial.println("flamme détecter dans la maison");
    delay(1000);
    Serial.println((char)26);
    delay(2000);
}

void detecteurfumeearretee()
{
    int fumee = digitalRead(A0);
    Serial.println(fumee);
    c = fumee;

    if(c==seuill)
    {
        digitalWrite(alarme, LOW);
        digitalWrite(led_pin_danger, LOW);
         
        Serial.println("fumee arrete");
        fumee_Set=0;

    }
}
void detecteurflammeeteint()
{
    int flamme = digitalRead(inputflamme_sensor_pin);

    Serial.println(flamme);

    d = flamme;

    if(d==seuill)
    {
        digitalWrite(alarme, LOW);
        digitalWrite(led_pin_danger, LOW);

        flamme_Set = 0;
    }
} 
