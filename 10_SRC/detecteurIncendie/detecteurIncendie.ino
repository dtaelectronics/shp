#include<SoftwareSerial.h>

const int led_pin_fonction = 3;
const int led_pin_danger = 4;
const int inputfumee_sensor_pin = 6;
const int inputflamme_sensor_pin =7 ;
const int alarme = 5;

const int powerfumee_sensor_pin = 1;
const int powerflamme_sensor_pin = 2;

float const seuil = HIGH;
float const seuill = LOW;

int flamme_Set = 0;
int fumee_Set = 0;

float a;float b;float c;float d;


//int fumee_sensor_val ;
//int flamme_sensor_val ;

void setup(){
    Serial.begin (19200);

    pinMode (led_pin_danger, OUTPUT);  //configuration pin en sortie
    pinMode (led_pin_fonction, OUTPUT);
    digitalWrite (led_pin_fonction, HIGH);
    
    pinMode (inputfumee_sensor_pin, INPUT);   //configuration pin en entree
    pinMode (powerfumee_sensor_pin, OUTPUT);  //configuration pin en sortie
    digitalWrite (powerfumee_sensor_pin, HIGH);

    pinMode (inputflamme_sensor_pin, INPUT); //configuration pin en entree
    pinMode (powerflamme_sensor_pin, OUTPUT);    //configuration pin en sortie
    digitalWrite (powerflamme_sensor_pin, HIGH);  //nous donnons un etat de sortie
    
    pinMode (alarme, OUTPUT);  //configuration pin en sortie

    delay(1000);

    Serial.println("composants initialise");
}

void loop()
{
    detecteurincendi();
    detecteurincendietient();
}

void detecteurincendi() //declarer une fonction principal 
{
    detecteurflamme();
    detecteurfumee();
}

void detecteurincendietient()   //declarer une fonction principal
{
    if(flamme_Set==1)
    {
        detecteurflammeeteint();    //declarer une fonction pour detecter si la flamme est eteint
    }
    else if(fumee_Set==1)
    {
        detecteurfumeearretee();  //declarer une fonction pour detecter si la fumee est arrete
    }
}

void detecteurflamme()
{
    int flamme = digitalRead(inputflamme_sensor_pin);
    Serial.println(flamme);

    a = flamme;

    if(a==seuil)
    {
        digitalWrite(alarme, HIGH);
        Alertflamme();
        flamme_Set = 1;
    }
    delay(1000);
}
void detecteurfumee()
{
    int fumee = digitalRead(A0);
    Serial.println(fumee);

    b = fumee;
    if(b==seuil)
    {
        digitalWrite(alarme, HIGH);
        Alertfumee();

        fumee_Set=1;
    }
}

void Alertfumee()
{
    digitalWrite(led_pin_danger, HIGH);
    affichagefumee();
}

void Alertflamme()
{
    digitalWrite(led_pin_danger, HIGH);
    affichageflamme();
}

void affichagefumee()
{
  
}

void affichageflamme()
{
  
}

void detecteurfumeearretee()
{
    int fumee = digitalRead(A0);
    Serial.println(fumee);
    c = fumee;

    if(c==seuill)
    {
        digitalWrite(alarme, LOW);
        digitalWrite(led_pin_danger, LOW);
         
        Serial.println("fumee arrete");
        fumee_Set=0;

    }
}
void detecteurflammeeteint()
{
    int flamme = digitalRead(inputflamme_sensor_pin);

    Serial.println(flamme);

    d = flamme;

    if(d==seuill)
    {
        digitalWrite(alarme, LOW);
        digitalWrite(led_pin_danger, LOW);

        flamme_Set = 0;
    }
}
