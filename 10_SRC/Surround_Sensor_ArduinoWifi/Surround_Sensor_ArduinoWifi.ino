#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <DHT.h>
#include <Adafruit_BMP085.h>
#include <ESP8266WiFi.h>
#include "Utilities.h"

const char* ssid = "WiFi_CleverTree";
const char* passwd = "02655826";
const char* host = "192.168.1.200";
const int port = 50011;

IPAddress local_IP(192, 168, 1, 211);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
word ui16_counter = 0;


DHT dht(DHTPIN, DHTTYPE);
LiquidCrystal_I2C lcd(0x27, 20, 4);
WiFiClient m_c_WiFiClient;
bool b_IsConnected = false;

float f_temperature= 0f, f_humidity= 0f;
double d_temp, d_humidity, d_UV;
int ui16_Mois1, ui16_Mois2, ui16_Mois3, ui8_WaterLevel, ui16_Visible, ui16_IR, ui16_CO, ui16_LPG, ui16_Smoke;
boolean b_IsPumpActivated;
double aui16_ArrayValue[MAX_ARRAY_LENGTH + 1] = {0};
int aui8_IndexComa[MAX_ARRAY_LENGTH] = {0};
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  void
  ----------------------------------------------------------------
  @parameter:   --
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void setup()
{
  dht.begin();
  v_InitDisplay();
  Serial.begin(BAUDRATE);
  Serial.println("Client");
  Serial.println("Connecting to Network");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passwd);

  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  } else {
    Serial.println("STA succesfully configured");
  }
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(100);
  }
  Serial.print("\n [CLIENT1 CONNECT] ");
  Serial.println(WiFi.localIP());
  delay(10000);
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  loop
  ----------------------------------------------------------------
  @parameter:   --
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void loop()
{
  ui16_counter++;
  if(ui16_counter == 60000)
  {
    ui16_counter = 0;
  }
  getTempAndHum(f_temperature, f_humidity);
  if(ui16_counter % 2 == 0)
  {
    v_PrintDisplay_1();
  }
  else
  {
    v_PrintDisplay_2();
  }
  
  v_ClientReadSendMessage(f_temperature, f_humidity);
  delay(60000);
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  reads data from DHT11 sensor
  ----------------------------------------------------------------
  @parameter:   float &f_temp, float &f_hum
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void getTempAndHum(float &f_temp, float &f_hum)
{
  f_hum = dht.readHumidity();
  f_temp = dht.readTemperature();
  if (isnan(f_temp) || isnan(f_hum))
  {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  send _Message with the current Moisture
  sensor value to Server
  ----------------------------------------------------------------
  @parameter:   String buf
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void v_ClientReadSendMessage(float f_temp, float f_hum)
{
  tChar recv_buf[100];
  tChar buf[5];
  String s_IncomingMsg;
  sprintf(buf, "%d,%d", f_temp, f_hum);
  m_c_WiFiClient.print(buf);

  while (m_c_WiFiClient.available())
  {
    s_IncomingMsg = m_c_WiFiClient.readStringUntil('\r');
  }
  sprintf(recv_buf, "%s", s_IncomingMsg);
  Serial.println("Incoming Data from all Sensors:");
  Serial.println("-------------------------------------------------");
  Serial.println(recv_buf);
  Serial.println("-------------------------------------------------");

  v_DataParser(s_IncomingMsg, b_IsPumpActivated, ui16_IR, ui16_Visible, ui8_WaterLevel,
               ui16_Mois1, ui16_Mois2, ui16_Mois3, d_temp, d_humidity, d_UV, ui16_CO, ui16_LPG, ui16_Smoke);
  char printbuf[255];
  sprintf(printbuf, "Data after Parsen:\n pump is activated:%d, IR:%d, VISIBLE:%d,"\
          "Water LEVEL:%d, MOIS1:%d \nMOIS2:%d, MOIS3:%d, TEMP:%s, HUM:%s, UV:%s, CO:%d, LPG:%d, Smoke:%d", \
          b_IsPumpActivated, ui16_IR, ui16_Visible, ui8_WaterLevel, ui16_Mois1, ui16_Mois2, \
          ui16_Mois3, String(d_temp, 2).c_str(), String(d_humidity, 2).c_str(), String(d_UV, 4).c_str(),\
          ui16_CO, ui16_LPG, ui16_Smoke);
  Serial.println(printbuf);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  init display
  ----------------------------------------------------------------
  @parameter:   --
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void v_InitDisplay()
{
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("    Clever Tree");
  lcd.setCursor(0, 3);
  lcd.print("     DTTA");
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  print a character at the display
  ----------------------------------------------------------------
  @parameter:   
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void v_PrintDisplay_1()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("TEMP: ");              //   01234567890123456789
  lcd.setCursor(6, 0);              //  ----------------------
  lcd.print(f_temperature);         //  |TEMP: 23.02C CO:2803|
  lcd.setCursor(11, 0);             //  |HUM: 55.00% LPG: 4  |
  lcd.print("C");                   //  |IR: 17  UV: 0.0403  |
  lcd.setCursor(0, 1);              //  |W-Level: 90%        |
  lcd.print("HUM: ");               //  ----------------------
  lcd.setCursor(5, 1);
  lcd.print(f_humidity);
  lcd.setCursor(10, 1);
  lcd.print("%");
  lcd.setCursor(0, 2);
  lcd.print("IR: ");
  lcd.setCursor(4, 2);
  lcd.print(ui16_IR);
  lcd.setCursor(8, 2);
  lcd.print("UV: ");
  lcd.setCursor(12, 2);
  lcd.print(d_UV);
  lcd.setCursor(0, 3);
  lcd.print("W-Level: ");
  lcd.setCursor(12, 3);
  lcd.print(ui8_WaterLevel);
  lcd.setCursor(16, 3);
  lcd.print( "%");
  lcd.setCurSor(13, 0);
  lcd.print("CO:");
  lcd.setCurSor(16, 0);
  lcd.print(ui16_CO);
  lcd.setCurSor(12, 1);
  lcd.print("LPG:");
  lcd.setCurSor(17, 1);
  lcd.print(ui16_LPG);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  print a character at the display
  ----------------------------------------------------------------
  @parameter:   
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void v_PrintDisplay_2()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("MOIS1: ");            //   01234567890123456789
  lcd.setCursor(7, 0);             //  ----------------------
  lcd.print(ui16_Mois1);           //  |MOIS1: 1023         |
  lcd.setCursor(0, 1);             //  |MOIS2: 1023         |
  lcd.print("MOIS2: ");            //  |MOIS3: 1023         |
  lcd.setCursor(7, 1);             //  |PUMP: OFF           |
  lcd.print(ui16_Mois2);           //  ----------------------
  lcd.setCursor(0, 2);
  lcd.print("MOIS3: ");
  lcd.setCursor(7, 2);
  lcd.print(ui16_Mois3);
  lcd.setCursor(0, 3);
  lcd.print("PUMP: ");
  lcd.setCursor(6, 3);
  if(b_IsPumpActivated == 0)
  {
    lcd.print("OFF");
  }
  else
  {
    lcd.print("ON");
  }
  
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  parsen data
  ----------------------------------------------------------------
  @parameter:   fString c_String, bool& b_pumpIsActivated, int& ui16_IR,
   int& ui16_VISIBLE, int& ui8_Waterlevel, int& mois1, int& mois2,
    int& mois3, double& temp,
		double& hum, double& uv
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid v_DataParser(String c_String, bool& b_rc_pumpIsActivated, int& ui16_rc_IR, int& ui16_rc_VISIBLE, int& ui8_rc_Waterlevel, int& rc_mois1, int& rc_mois2, int& rc_mois3, double& rc_temp,
                   double& rc_hum, double& rc_uv, int& ui16_rc_CO, int& ui16_rc_LPG, int& ui16_rc_Smoke)
{
  //"0,90,234,435,1023,23.4,56.0,6,0.043,234"
  int ui8_Index = 1;


  for (int ui8_pos = 0; ui8_pos < (int)c_String.length(); ui8_pos++ )
  {
    if (c_String[ui8_pos] == ',')
    {
      aui8_IndexComa[ui8_Index] = ui8_pos;;
      ui8_Index++;
    }
  }

  for (int ui8_pos = 0; ui8_pos < MAX_ARRAY_LENGTH + 1; ui8_pos++)
  {
    Serial.println(aui8_IndexComa[ui8_pos]);
    if (ui8_pos == 0)
    {
      String s = c_String.substring(0, 1);
      aui16_ArrayValue[ui8_pos] = s.toDouble();
      Serial.print("s: ");
      Serial.println(s);
    }
    else if (ui8_pos < MAX_ARRAY_LENGTH)
    {
      String s = c_String.substring(aui8_IndexComa[ui8_pos] + 1, aui8_IndexComa[ui8_pos + 1]);
      aui16_ArrayValue[ui8_pos] = s.toDouble();
      Serial.print("s: ");
      Serial.println(s);
    }
    else if (ui8_pos == MAX_ARRAY_LENGTH)
    {
      String s = c_String.substring(aui8_IndexComa[ui8_pos] + 1, (int)c_String.length());
      aui16_ArrayValue[ui8_pos] = s.toDouble();
      Serial.print("s: ");
      Serial.println(s);
    }

  }
  b_rc_pumpIsActivated = aui16_ArrayValue[0];
  ui8_rc_Waterlevel = (int)aui16_ArrayValue[1];
  rc_mois1 =  aui16_ArrayValue[2];
  rc_mois2 = aui16_ArrayValue[3];
  rc_mois3 = aui16_ArrayValue[4];
  rc_temp = aui16_ArrayValue[5];
  rc_hum = aui16_ArrayValue[6];
  ui16_rc_IR = aui16_ArrayValue[7];
  rc_uv = aui16_ArrayValue[8];
  ui16_rc_VISIBLE = (int)aui16_ArrayValue[9];
  ui16_rc_CO = aui16_ArrayValue[10];
  ui16_rc_LPG = aui16_ArrayValue[11];
  ui16_rc_Smoke = aui16_ArrayValue[12];
}
