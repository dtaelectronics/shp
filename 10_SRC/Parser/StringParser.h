/*
 * StringParser.h
 *
 *  Created on: 03.07.2022
 *      Author: emger
 */



#ifndef __STRINGPARSER_H__
#define __STRINGPARSER_H__

#include "stdint.h"
#include <stdio.h>
#include <iostream>
#include <string>

typedef void tVoid;
typedef uint8_t tUInt8;
typedef uint16_t tUInt16;

#define MAX_ARRAY_LENGTH 9

class StringParser
{

public:
    StringParser();
    ~StringParser();
    tVoid v_DataParser(std::string c_String, bool& pumpIsActivated, int& ui16_IR, int& ui16_VISIBLE, int& ui8_Waterlevel, int& mois1, int& mois2, int& mois3, double& temp,
    		double& hum, double& uv);


};

#endif //__STRINGPARSER_H__


