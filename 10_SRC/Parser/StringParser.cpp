/*
 * StringParser.cpp
 *
 *  Created on: 03.07.2022
 *      Author: emger
 */
#include "StringParser.h"





StringParser::StringParser()
{

}

StringParser::~StringParser()
{

}


tVoid StringParser::v_DataParser(std::string c_String, bool& pumpIsActivated, int& ui16_IR, int& ui16_VISIBLE, int& ui8_Waterlevel, int& mois1, int& mois2, int& mois3, double& temp,
		double& hum, double& uv)
{
	//"0,90,234,435,1023,23.4,56.0,6,0.043,234"
    int ui8_Index = 1;
    double aui16_ArrayValue[MAX_ARRAY_LENGTH+1] ={0};
    int aui8_IndexComa[MAX_ARRAY_LENGTH] ={ 0 };

    for(int ui8_pos = 0; ui8_pos < (int)c_String.length(); ui8_pos++ )
    {
    	if(c_String[ui8_pos] == ',')
    	{
    		aui8_IndexComa[ui8_Index] = ui8_pos;
    		ui8_Index++;
    	}
    }

    for(int ui8_pos = 0; ui8_pos < MAX_ARRAY_LENGTH+1; ui8_pos++)
    {
    	if(ui8_pos == 0)
    	{
    		std::string s = c_String.substr(0, 1);
    		aui16_ArrayValue[ui8_pos] = std::stod(s);
    		//std::cout << s << std::endl;
    	}
    	else if(ui8_pos < MAX_ARRAY_LENGTH)
    	{
    		std::string s = c_String.substr(aui8_IndexComa[ui8_pos]+1, aui8_IndexComa[ui8_pos+1]-aui8_IndexComa[ui8_pos]-1);
    		aui16_ArrayValue[ui8_pos] = std::stod(s);
    		//std::cout << s << std::endl;
    	}
    	else if(ui8_pos == MAX_ARRAY_LENGTH)
    	{
    		std::string s = c_String.substr(aui8_IndexComa[ui8_pos]+1, (int)c_String.length()-aui8_IndexComa[ui8_pos]-1);
    		aui16_ArrayValue[ui8_pos] = std::stod(s);
    		//std::cout << s << std::endl;
    	}

    }
    pumpIsActivated = aui16_ArrayValue[0];
    ui8_Waterlevel = (int)aui16_ArrayValue[1];
    mois1 = aui16_ArrayValue[2];
    mois2 = aui16_ArrayValue[3];
    mois3 = aui16_ArrayValue[4];
    temp = aui16_ArrayValue[5];
    hum = aui16_ArrayValue[6];
    ui16_IR = aui16_ArrayValue[7];
    uv = aui16_ArrayValue[8];
    ui16_VISIBLE = (int)aui16_ArrayValue[9];
}



