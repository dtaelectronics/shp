/*
* This Class instanzied a Analog Pin 
*/

#include "Arduino.h"
#include "Utilities.h"


class DigitalPin
{

    public:
        DigitalPin(tUInt8 ui8_DigitalPin, tBool b_PinMode);
        bool b_ReadDigitalPin();
        void v_WriteDigitalPin(tBool b_WriteValue);
        tBool v_InitDigitalPin();

    private:
        tUInt8 m_ui8_DigitalPin;
        tBool m_b_PinMode;
};