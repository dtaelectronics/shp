#include "Utilities.h"
#include "FireSensor.h"

FireSensor m_c_FireSensor(FIRESENSOR_ANALOGPIN, FIRESENSOR_DIGITALPIN);
tUInt16 ui16_AnalogValue_FireSensor = 0;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  setup
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  --

----------------------------------------------------------------
@parameter:   --
@Returnvalue: --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void setup() {
  v_Init();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function: loop
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description: --

----------------------------------------------------------------
@parameter:   --
@Returnvalue: --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void loop() {
  v_FireSensorHandler();
  delay(20);        // each 20 ms 
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  v_Init
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description: initializes all Pins and Serial Communication

----------------------------------------------------------------
@parameter: 
@Returnvalue: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void v_Init() {
  tBool b_Check = false;
  do
  {
    Serial.begin(BAUDRATE);
    b_Check = m_c_FireSensor.v_Init();
    pinMode(LED_PIN, OUTPUT);
    Serial.println("Init: Wait...");
    delay(100);
  } while (!b_Check);

  Serial.println("Init: Successful");
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  v_FireSensorHandler
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  Handles fire sensor, If the Sensor detects
a fire then the LED should be light up

----------------------------------------------------------------
@parameter:   --
@Returnvalue: --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void v_FireSensorHandler()
{
    tBool b_DigitalValue = m_c_FireSensor.b_ReadValueFromFireSensor(ui16_AnalogValue_FireSensor);
    if(b_DigitalValue)
    {
      digitalWrite(LED_PIN, HIGH);
    }
    else
    {
      digitalWrite(LED_PIN, LOW);
    }
    Serial.print("FireSensor: Fire is detected = ");
    Serial.println(b_DigitalValue);
}
