#include <stdint.h>
#include <stdio.h>

#define BAUDRATE                115200
#define FIRESENSOR_ANALOGPIN    A0
#define FIRESENSOR_DIGITALPIN   3
#define LED_PIN                 13

typedef uint8_t     tUInt8;
typedef uint16_t    tUInt16;
typedef boolean     tBool;
typedef uint32_t    tUInt32;
typedef uint64_t    tUInt64;
typedef char        tChar;


