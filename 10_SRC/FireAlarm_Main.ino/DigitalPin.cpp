#include "DigitalPin.h"


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  DigitalPin
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  Constructor
    - INPUT     (0x0)
    - OUTPUT    (0x1)
----------------------------------------------------------------
@parameter:     tUInt8 ui8_DigitalPin, tBool b_PinMode
@Returnvalue:   --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
DigitalPin::DigitalPin(tUInt8 ui8_DigitalPin, tBool b_PinMode)
: m_ui8_DigitalPin(ui8_DigitalPin)
, m_b_PinMode(b_PinMode)
{
    
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  v_InitDigitalPin
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  initializes a pin to digital pin

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   true  -- If the pin number less equal than 13
                false -- otherwise
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tBool DigitalPin::v_InitDigitalPin()
{
    tBool b_RetVal = false;
    if(m_ui8_DigitalPin < 14)
    {
        pinMode(m_ui8_DigitalPin, (int)m_b_PinMode);
        b_RetVal = true;
    }
    return b_RetVal;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  b_ReadDigitalPin
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  read value from digital pin

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   tBool b_RetVal
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tBool DigitalPin::b_ReadDigitalPin()
{
    tBool b_RetVal = digitalRead(m_ui8_DigitalPin);
    return b_RetVal;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  v_WriteDigitalPin
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  write Value to digital pin

----------------------------------------------------------------
@parameter:     tBool b_WriteValue
@Returnvalue:   --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void DigitalPin::v_WriteDigitalPin(tBool b_WriteValue)
{
    digitalWrite(m_ui8_DigitalPin, b_WriteValue);
}