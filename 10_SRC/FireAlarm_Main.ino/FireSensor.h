#include "Arduino.h"
#include "Utilities.h"
#include "AnalogPin.h"
#include "DigitalPin.h"


class FireSensor
{
    public:
        FireSensor(tUInt8 ui8_AnalogPin, tUInt8 ui8_DigitalPin);
        tBool b_ReadValueFromFireSensor(tUInt16 &ui16_AnalogValue);
        tBool v_Init();

    private:
        DigitalPin m_c_DigitalPin;
        AnalogPin m_c_AnalogPin;
        tUInt16 ui16_ReadAnalogValue();
        tBool b_ReadDigitalValue();

};