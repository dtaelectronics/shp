#include "FireSensor.h"


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  FireSensor
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  Constructor

----------------------------------------------------------------
@parameter: tUInt8 ui8_AnalogPin, tUInt8 ui8_DigitalPin
@Returnvalue: --
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
FireSensor::FireSensor(tUInt8 ui8_AnalogPin, tUInt8 ui8_DigitalPin)
: m_c_AnalogPin(ui8_AnalogPin)
, m_c_DigitalPin(ui8_DigitalPin, 0)
{
    
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  ui16_ReadAnalogValue
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  reads value from analog pin fire sensor

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   m_c_AnalogPin.ui16_ReadAnalogPin()
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tUInt16 FireSensor::ui16_ReadAnalogValue()
{
    return m_c_AnalogPin.ui16_ReadAnalogPin();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  b_ReadDigitalValue
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  reads value from digital pin fire sensor

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   m_c_DigitalPin.b_ReadDigitalPin()
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tBool FireSensor::b_ReadDigitalValue()
{
    return m_c_DigitalPin.b_ReadDigitalPin();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  b_ReadValueFromFireSensor
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 23.04.2022
----------------------------------------------------------------
@Function Description:  reads value from digital and Analog
pin for fire sensor and returns this
----------------------------------------------------------------
@parameter:     tUInt16 &ui16_AnalogValue
@Returnvalue:   b_ReadDigitalValue()
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tBool FireSensor::b_ReadValueFromFireSensor(tUInt16 &ui16_AnalogValue)
{
    ui16_AnalogValue = ui16_ReadAnalogValue();
    return b_ReadDigitalValue();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Function:  v_Init
@Author:    Gerald Emvoutou | Digital Transformation & Technology 
                                Alliance
@Creation: 18.04.2022
----------------------------------------------------------------
@Function Description:  initializes the firesensor

----------------------------------------------------------------
@parameter:     --
@Returnvalue:   true -- successful
                false -- failed
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tBool FireSensor::v_Init()
{
    if(m_c_DigitalPin.v_InitDigitalPin())
    {
        return true;
    }
    else
    {
        Serial.println("FireSensor: Init FireSensor failed");
        return false;
    }
    
    
}

