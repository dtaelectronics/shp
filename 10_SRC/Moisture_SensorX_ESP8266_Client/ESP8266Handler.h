#include "Arduino.h"
#include "Utilities.h"
#include <ESP8266WiFi.h>


class ESP8266Handler
{

    public:
        ESP8266Handler( IPAddress c_IPAddressLocalIP );

        tBool b_InitESP8266();
        tBool b_IsConnect(tBool& b_IsConnect);
        tVoid v_InitClient(WiFiClient c_WiFiClient);
        tVoid v_SendDataToServer(tString s_DataToSend, tBool& b_IsConnect);

    private:
        const tChar* m_pc_ssid;
        const tChar* m_pc_password;
        const tChar* m_pc_host;
        const tUInt16 m_ui16_port;
        tBool m_b_IsConnect;
        IPAddress m_c_IPAddressGateway;
        IPAddress m_c_IPAddressSubnet;
        IPAddress m_c_IPAddressLocalIP;
        WiFiClient* m_pc_WiFiClient;
};
