#include <Arduino.h>
#include <stdint.h>
#include <stdio.h>

#define BAUDRATE                115200
#define sSID                    "WiFi_CleverTree"
#define PSSWD                   "02655826"
#define HOST                    "192.168.1.200"
#define PORT                    50011
#define MQ2_PIN                 A0
#define GATEWAY_ADDR            (192, 168, 1, 1)
#define SUBNET_ADDR             (255, 255, 255, 0)
#define TIMEOUT                 30000
#define TIMER_5_MIN             3000//00
#define TIMER_20_MS             20

typedef uint8_t     tUInt8;
typedef uint16_t    tUInt16;
typedef boolean     tBool;
typedef uint32_t    tUInt32;
typedef uint64_t    tUInt64;
typedef char        tChar;
typedef void        tVoid;
typedef String      tString;
