/* ****************************
   @brief

   @author Gerald Cezy Emvoutou

   @date 2022-09-07

   @copyright Digital Transformation & Technology Alliance

*/

#include <ESP8266WiFi.h>
#include "Utilities.h"
#include "SI114X.h"
#include "Wire.h"
#include "MQ2.h"

const tChar* ssid = "WiFi_CleverTree";
const tChar* passwd = "02655826";
const tChar* host = "192.168.1.200";
const tUInt16 port = 50011;

//change the local ip for each ESP8266
IPAddress local_IP(192, 168, 1, 204);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

MQ2 m_c_MQ2Sensor(MQ2_PIN);
SI114X m_c_SunLightSensor;
WiFiClient m_c_WiFiClient;
tBool b_IsConnected = false;
int ui16_IR = 0, u16_Visible= 0, ui16_CO= 0, ui16_LPG= 0, ui16_Smoke= 0;
float f_Uv= 0f;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  setup

  ----------------------------------------------------------------
  @parameter:     --
  @Returnvalue:
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid setup()
{
  Serial.begin(BAUDRATE);
  Serial.println("Client");
  Serial.println("Connecting to Network");

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passwd);

  if (!WiFi.config(local_IP, gateway, subnet)) 
  {
    Serial.println("STA Failed to configure");
  } 
  else 
  {
    Serial.println("STA succesfully configured");
  }

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(100);
  }

  Serial.print("\n [CLIENT1 CONNECT] ");
  Serial.println(WiFi.localIP());
  m_c_MQ2Sensor.begin();
  if(m_c_SunLightSensor.Begin())
  {
    Serial.println("Sunlight Sensor ready");
  }
  else
  {
    Serial.println("Sunlight Sensor not ready");
  }
  delay(10000); // wait 10 second
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  loop

  ----------------------------------------------------------------
  @parameter:     --
  @Returnvalue:
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid loop()
{
    tChar buf[64];

    if (!b_IsConnected) {
        while (!m_c_WiFiClient.connect(host, port)) {
        Serial.println("Waiting to connect");
        delay(500);
        }
        b_IsConnected = true;
        Serial.println("Connection successfully");
    }

    v_ReadDataFromSunlightSensor(ui16_IR, f_Uv, u16_Visible);
    v_ReadDataFromMQ2Sensor(ui16_CO, ui16_LPG, ui16_Smoke);
    sprintf(buf, "%d,%s,%d,%d,%d,%d", ui16_IR, String(f_Uv,4).c_str(), u16_Visible, ui16_CO, ui16_LPG, ui16_Smoke);
    v_ClientSendMessage(buf);
    delay(1000);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @Author:    Gerald Emvoutou | Digital Transformation & Technology
                                Alliance
  @Creation: 24.04.2022
  ----------------------------------------------------------------
  @Function Description:  send _Message with the current Moisture
  sensor value to Server
  ----------------------------------------------------------------
  @parameter:   String buf
  @Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid v_ClientSendMessage(String s_buf)
{
  m_c_WiFiClient.print(s_buf);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Author:    Gerald Emvoutou | Digital Transformation & Technology
                            Alliance
@Creation: 09.07.2022
----------------------------------------------------------------
@Function Description:  read data from sunlight sensor
----------------------------------------------------------------
@parameter:   int& ui16_rc_IR, float& f_rc_UV, int& ui16_rc_Visible
@Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid v_ReadDataFromSunlightSensor(int& ui16_rc_IR, float& f_rc_UV, int& ui16_rc_Visible)
{
    ui16_rc_IR = m_c_SunLightSensor.ReadIR();
    f_rc_UV = m_c_SunLightSensor.ReadUV();
    ui16_rc_Visible = m_c_SunLightSensor.ReadVisible();
}
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@Author:    Gerald Emvoutou | Digital Transformation & Technology
                            Alliance
@Creation: 09.07.2022
----------------------------------------------------------------
@Function Description:  read data from MQ2 sensor
----------------------------------------------------------------
@parameter:   int& ui16_rc_CO, int& ui16_rc_LPG, int& ui16_rc_Smoke
@Returnvalue:   --
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
tVoid v_ReadDataFromMQ2Sensor(int& ui16_rc_CO, int& ui16_rc_LPG, int& ui16_rc_Smoke)
{
    ui16_rc_CO = m_c_MQ2Sensor.readCO();
    ui16_rc_LPG = m_c_MQ2Sensor.readLPG();
    ui16_rc_Smoke = m_c_MQ2Sensor.readSmoke();
}