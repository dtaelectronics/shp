# 1 "c:\\DTTA\\shp\\10_SRC\\test_motor\\test_motor.ino"
/**

 * @file         Sabertooth 2 X 25A Motor Driver.ino

 * @author       IntoRobotics.com

 * @version      V1.0

 * @date         2016/06/03

 * @description  this file is sample code for the Sabertooth 2 X 25A Motor Driver and Arduino UNO

 */
# 10 "c:\\DTTA\\shp\\10_SRC\\test_motor\\test_motor.ino"
/*Sending a character between 1 and 127 will control motor 1.

1 is full reverse, 64 is stop and 127 is full forward.

Sending a character between 128 and 255 will control motor 2.

128 is full reverse, 192 is stop and 255 is full forward.

Character 0 (hex 0x00) is a special case. Sending this character will shut down both motors.

Source:http://www.robotmarketplace.com/products/images/Sabertooth2x25.pdf

*/
# 19 "c:\\DTTA\\shp\\10_SRC\\test_motor\\test_motor.ino"
//simplifierd serial limits for each motor






//shut down both motors



void setup() {
  Serial.begin(9600);
  killMotors();
}

void loop() {
  Serial.write(1);
}

 void fastForward(){ //motors fast forward
    Serial.write(127);
    Serial.write(255);
    Serial.println("motors fast forward");
  }


 void fastReverse(){ //motors fast reverse
    Serial.write(1);
    Serial.write(128);
    Serial.println("motors fast reverse");
  }

  void turnLeft(){ //motor 1 full reverse and motor 2 full forward to turn left
    Serial.write(1);
    Serial.write(255);
    Serial.println("motor 1 full reverse and motor 2 full forward to turn left");
  }

  void turnRight(){ //motor 1 full forward and motor 2 full reverse to turn right
    Serial.write(127);
    Serial.write(128);
    Serial.println("motor 1 full forward and motor 2 full reverse to turn right");
  }

  void killMotors(){
    Serial.write(0); //kill motors for 0.5 second
    Serial.write(0); //kill motors for 0.5 second
    Serial.println("kill motors for half a second");
    delay(500);
    }
